package main

import (
	"fmt"
	"log"
	"net/http"

	"time"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	"encoding/json"
)

var server = "157.179.28.22"
var port = 1433
var user = "sa"
var password = "dioro@4321"
var database = "GISCCommission"

type Test struct {
	Name string `json:"test"`
}

type ProjectFromPTS struct {
	ID            int       `gorm:"column:ID"`
	ContractNO    string    `gorm:"column:ContractNo"`
	ProjectName   string    `gorm:"column:ProjectName"`
	ProjecDesc    string    `gorm:"column:ProjectDesc"`
	SaleID        string    `gorm:"column:SaleID"`
	SaleName      string    `gorm:"column:SaleName"`
	ContType      string    `gorm:"column:ContType"`
	CustomerNo    string    `gorm:"column:CustomerNo"`
	ThaiEndUser   string    `gorm:"column:Thai_End_User"`
	EngEndUser    string    `gorm:"column:Eng_End_User"`
	SignDate      time.Time `gorm:"column:SignDate"`
	ProjStart     time.Time `gorm:"column:ProjStart"`
	EndContract   time.Time `gorm:"column:EndContract"`
	PRODGRPNAME   string    `gorm:"column:PRODGRPNAME"`
	ContKindName  string    `gorm:"column:ContKindName"`
	WaStartDate   time.Time `gorm:"column:WaStartDate"`
	WaEndDate     time.Time `gorm:"column:WaEndDate"`
	NETSALE       float64   `gorm:"column:NETSALE"`
	VATAMOUNT     float64   `gorm:"column:VATAMOUNT"`
	ESRISW        float64   `gorm:"column:ESRISW"`
	ComRate       float32   `gorm:"column:ComRate"`
	Margin        float32   `gorm:"column:Margin"`
	flagComRate   string    `gorm:"column:flagComRate"`
	UserUpdatePTS string    `gorm:"column:UserUpdatePTS"`
	UpdateDatePTS time.Time `gorm:"column:UpdateDatePTS"`
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello world")
}

func Projects(w http.ResponseWriter, r *http.Request) {

	var err error

	conString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s",
		server, user, password, port, database)

	db, err := gorm.Open("mssql", conString)
	if err != nil {
		log.Fatal("Error creating connection pool: " + err.Error())
	}

	var projects []ProjectFromPTS
	db.Table("ProjectFromPTS").Find(&projects)
	json.NewEncoder(w).Encode(projects)

}

func Project(w http.ResponseWriter, r *http.Request) {
	var err error
	vars := mux.Vars(r)

	conString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s",
		server, user, password, port, database)

	db, err := gorm.Open("mssql", conString)
	if err != nil {
		log.Fatal("Error creating connection pool: " + err.Error())
	}

	var projects []ProjectFromPTS
	db.Table("ProjectFromPTS").Where("ContractNo = ?", vars["contractNO"]).Find(&projects)
	json.NewEncoder(w).Encode(projects)
}

func AddProject(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var t Test
	_ = json.NewDecoder(r.Body).Decode(&t)

	json.NewEncoder(w).Encode(t)
}

func EditUser(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "edit")
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "delete")
}

func handlerRequests() {

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/projects", Projects).Methods("GET")
	myRouter.HandleFunc("/project/{contractNO}", Project).Methods("GET")
	myRouter.HandleFunc("/project", AddProject).Methods("POST")
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}

func main() {
	time.Now()
	/* This is my first sample rest api. */
	var err error

	conString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s",
		server, user, password, port, database)

	db, err := gorm.Open("mssql", conString)
	if err != nil {
		log.Fatal("Error creating connection pool: " + err.Error())
	}
	log.Printf("Connected!\n")
	// gorm.DefaultCallback.Create().Remove("mssql:set_identity_insert")
	defer db.Close()

	handlerRequests()
}
